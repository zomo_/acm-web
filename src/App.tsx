import TestPage from './components/TestPage'
import LetuHeader from './components/LetuHeader'
import Letufooter from './components/LetuFooter'
import { Helmet } from 'react-helmet'

export default function App() {
    return (
        <>
            <Helmet>
                <title>LeTourneau ACM</title>
            </Helmet>

            <LetuHeader />
            <TestPage />
            <Letufooter />
        </>
    )
}
