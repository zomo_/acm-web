import logoLetu from '../assets/logos/LETUShield/LETUShield-FllClrBlue.svg'
import logoACM from '../assets/logos/ACM-Standalone/ACM.svg'
import { createRef, useEffect, useState } from 'react'

export default function Letuheader() {
    const headRef = createRef<HTMLDivElement>()
    const [offsetTrigger, setOffsetTrigger] = useState(0)
    const [offset, setOffset] = useState(window.scrollY)

    // scroll effect
    useEffect(() => {
        const onScroll = () => setOffset(window.scrollY)

        window.removeEventListener('scroll', onScroll)
        window.addEventListener('scroll', onScroll, { passive: true })
        return () => window.removeEventListener('scroll', onScroll)
    }, [])

    // scroll offset trigger
    useEffect(() => {
        const box = headRef.current?.getBoundingClientRect()
        if (box) {
            setOffsetTrigger(box.height / 2)
        }
    })

    // conditional elements
    return (
        <div className="w-full h-32" ref={headRef}>
            {offset <= offsetTrigger ? (
                <div className="bg-grey p-3 flex items-center w-full h-32">
                    <img src={logoLetu} className="h-24" />
                    <img src={logoACM} className="h-24" />
                </div>
            ) : (
                <div className="bg-grey p-3 flex items-center w-full fixed top h-16">
                    <img src={logoLetu} className="h-12" />
                    <img src={logoACM} className="h-12" />
                </div>
            )}
        </div>
    )
}
