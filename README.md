# LeTourneau ACM

This is the repository for LeTourneau's website for ACM.

For questions, email: `JakeRosch@letu.edu`

## Install

[Node.js](https://nodejs.org/en) is required, the current version is recommended.

By default, Node uses `npm`, but `pnpm` and `yarn` also work.

```bash
$ npm i
```

## Running

To start a local dev server, run

```bash
$ npm run dev
```

This will open a port on localhost for hot-reloading the website.
